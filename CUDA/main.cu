#include <cuda.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "../cjson/cJSON.h"

#define RANDOM_RANGE 1000
#define MAX_INT32 2147483647
#define TH_P_BLOCK 1024

struct Point{
    float x;
    float y;
    int color;
};
typedef struct Point Point;

__device__ float distance(Point *a, Point *b){
    return sqrt((b->x - a->x)*(b->x - a->x) + (b->y - a->y)*(b->y - a->y));
}

__global__ void updateOldK(Point *oldK, Point *centr, int k){
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    if(i < k){
        oldK[i].x = centr[i].x;
        oldK[i].y = centr[i].y; 
    } 
}

__global__ void colorPoints(Point *points, int n, Point *centr, int k){
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    if(i < n){
        float minDistance = MAX_INT32, newDistance;
        for(int j = 0; j < k; j++){
            newDistance = distance(&points[i], &centr[j]);
            if(newDistance < minDistance){
                points[i].color = centr[j].color;
                minDistance = newDistance;
            }
        }
    }
}

__global__ void globalSum(Point *points, int n, float *sumCoordX, float *sumCoordY, int *pointNumber, int k){
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    if(i < n){
        int pointColor = points[i].color - 1; //Legge il colore del punto i-esimo
        atomicAdd(&sumCoordX[pointColor], points[i].x);
        atomicAdd(&sumCoordY[pointColor], points[i].y); 
        atomicAdd(&pointNumber[pointColor], 1);     
    }
}

__global__ void updateCentroids(Point *centr, int k, float *sumCoordX, float *sumCoordY, int *pointNumber){
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    if(i < k){
        if(pointNumber[i] > 0){
            centr[i].x = sumCoordX[i]/pointNumber[i];
            centr[i].y = sumCoordY[i]/pointNumber[i];
            sumCoordX[i] = 0;
            sumCoordY[i] = 0;
            pointNumber[i] = 0;
        }
    }
}

__global__ void checkEnd(Point *centr, Point *oldK, int k, short int *stop){
    *stop = 1;
    for(int i = 0; i < k && *stop; i++){
        //printf("centr[%d].x=%f old[%d].x=%f centr[%d].y=%f old[%d].y=%f\n",i, centr[i].x, i, oldK[i].x, i, centr[i].y, i, oldK[i].y);//printf("centr[%d].x=%f old[%d].x=%f centr[%d].y=%f old[%d].y=%f\n",i, roundf(centr[i].x*100)/100, i, roundf(oldK[i].x*100)/100, i, roundf(centr[i].y*100)/100, i, roundf(oldK[i].y*100)/100);
        if(fabs(centr[i].x - oldK[i].x) > 0.01 || fabs(centr[i].y - oldK[i].y) > 0.01)
            *stop = 0;
    }
}

float float_rand(float min, float max){
    float scale = rand() / (float) RAND_MAX;
    return min + scale * (max - min);
}

void writeJSON(Point *points, int n, Point *centr, int k){
    //JSON
    cJSON *globalObj = cJSON_CreateObject(); //global object
    cJSON *pointArr = cJSON_CreateArray();
    cJSON *pointObj;

    for(int i = 0; i < n; i++){
        pointObj = cJSON_CreateObject();
        cJSON_AddNumberToObject(pointObj, "x", points[i].x);
        cJSON_AddNumberToObject(pointObj, "y", points[i].y);
        cJSON_AddNumberToObject(pointObj, "col", points[i].color);
        cJSON_AddItemToArray(pointArr, pointObj);
    }
    cJSON_AddItemToObject(globalObj, "points", pointArr);
    pointArr = cJSON_CreateArray();

    for(int i = 0; i < k; i++){
        pointObj = cJSON_CreateObject();
        //printf("x:%f\ty:%f\tcolor:%d\n", centr[i].x, centr[i].y, centr[i].color);
        cJSON_AddNumberToObject(pointObj, "x", centr[i].x);
        cJSON_AddNumberToObject(pointObj, "y", centr[i].y);
        cJSON_AddNumberToObject(pointObj, "col", centr[i].color);
        cJSON_AddItemToArray(pointArr, pointObj);
    }
    cJSON_AddItemToObject(globalObj, "centr", pointArr);

    char *json_str = cJSON_Print(globalObj);

    FILE *fp = fopen("../points.json", "w");
    if(fp == NULL){
        printf("Error: Unable to open the file.\n");
    }

    fputs(json_str, fp);
    fclose;
    cJSON_free(json_str);
    cJSON_Delete(globalObj);
}

int main(int argc, char *argv[]){
    int k; //Numero di centroidi
    int n; //Numero di punti
    int epochLimit = -1;

    if(argc > 1){
        n = atoi(argv[1]);
        k = atoi(argv[2]);
        if(argc == 4)
            epochLimit = atoi(argv[3]);
    }else{
        printf("Quanti punti si vuole generare?\n");
        scanf("%d", &n);

        printf("Quanti centroidi si vogliono?\n");
        scanf("%d", &k);

        printf("Inserire limite massimo di iterazioni (-1 se non si vuole nessun limite)\n");
        scanf("%d", &epochLimit);
    }

    if(epochLimit == -1)
        epochLimit = MAX_INT32;

    Point *points = (Point *) calloc(n, sizeof(Point));
    Point *centr = (Point *) calloc(k, sizeof(Point));

    //Generazione punti (da -RAND_MAX a RAND_MAX)
    for(int i = 0; i < n; i++){
        points[i].x = float_rand(-RANDOM_RANGE, RANDOM_RANGE);//(rand() % (100 - (-100) + 1)) + (-100);
        points[i].y = float_rand(-RANDOM_RANGE, RANDOM_RANGE);//(rand() % (100 - (-100) + 1)) + (-100); 
    }

    //Scelgo i centroidi a caso tra i punti generati
    for(int i = 0; i < k; i++){
        int selected = (rand() % ((n-1) - (0) + 1)) + (0);
        centr[i].x = points[selected].x;
        centr[i].y = points[selected].y;
        centr[i].color = i + 1;
    }

    //##################################################CUDA##################################################
    //copio l'array dei punti e dei centroidi nel device
    Point *dev_points, *dev_centr;
    cudaMalloc(&dev_points, n*sizeof(Point));
    cudaMalloc(&dev_centr, k*sizeof(Point));
    cudaMemcpy(dev_points, points, n*sizeof(Point), cudaMemcpyHostToDevice);
    cudaMemcpy(dev_centr, centr, k*sizeof(Point), cudaMemcpyHostToDevice);

    //alloco anche l'array di appoggio oldK per il controllo della fine delle iterazioni
    Point *dev_oldK;
    cudaMalloc(&dev_oldK, k*sizeof(Point));

    //alloco la variabile di stop per il loop
    short int *dev_stop, stop = 0;
    cudaMalloc(&dev_stop, sizeof(short int));

    //alloco gli array necessari all'aggiornamento dei centroidi
    float *dev_sumCoordX, *dev_sumCoordY;
    int *dev_pointNumber;
    cudaMalloc(&dev_sumCoordX, k*sizeof(float));
    cudaMalloc(&dev_sumCoordY, k*sizeof(float));
    cudaMalloc(&dev_pointNumber, k*sizeof(int));
    cudaMemset(dev_sumCoordX, 0, k*sizeof(float));
    cudaMemset(dev_sumCoordY, 0, k*sizeof(float));
    cudaMemset(dev_pointNumber, 0, k*sizeof(int));

    int epoch = 1; 

    while(!stop && epoch <= epochLimit){

        //Mi salvo all'inizio di ogni iterazione il valore dei centroidi, così
        //posso controllare se sono cambiati (e se non dovessero cambiare fine di k means)
        updateOldK<<<(k + TH_P_BLOCK - 1)/TH_P_BLOCK, TH_P_BLOCK>>>(dev_oldK, dev_centr, k);
        
        //aspetta che il device finisca
        cudaDeviceSynchronize();

        //Per ogni punto devo calcolare la distanza da ogni centroide e selezionare
        //quello a distanza minore
        colorPoints<<<(n + TH_P_BLOCK - 1)/TH_P_BLOCK, TH_P_BLOCK>>>(dev_points, n, dev_centr, k);

        //aspetta che il device finisca
        cudaDeviceSynchronize();

        //Raccolgo le somme parziali nella memoria global
        globalSum<<<(n + TH_P_BLOCK - 1)/TH_P_BLOCK, TH_P_BLOCK>>>(dev_points, n, dev_sumCoordX, dev_sumCoordY, dev_pointNumber, k);

        //aspetta che il device finisca
        cudaDeviceSynchronize();

        //Aggiornamento dei centroidi
        updateCentroids<<<(k + TH_P_BLOCK - 1)/TH_P_BLOCK, TH_P_BLOCK>>>(dev_centr, k, dev_sumCoordX, dev_sumCoordY, dev_pointNumber);

        //aspetta che il device finisca
        cudaDeviceSynchronize();
        
        checkEnd<<<1, 1>>>(dev_centr, dev_oldK, k, dev_stop);
        
        //aspetta che il device finisca
        cudaDeviceSynchronize();

        printf("Ended epoch %d\n", epoch);
        epoch++;
        cudaMemcpy(&stop, dev_stop, sizeof(short int), cudaMemcpyDeviceToHost);
    }

    cudaMemcpy(points, dev_points, n*sizeof(Point), cudaMemcpyDeviceToHost);
    cudaMemcpy(centr, dev_centr, k*sizeof(Point), cudaMemcpyDeviceToHost);

    cudaFree(dev_points);
    cudaFree(dev_centr);
    cudaFree(dev_oldK);
    cudaFree(dev_stop);
    cudaFree(dev_sumCoordX);
    cudaFree(dev_sumCoordY);
    cudaFree(dev_pointNumber);
    //########################################################################################################

    if (n < 50000) //Limite per scrivere il json, altrimenti usa troppa memoria
        writeJSON(points, n, centr, k);

    free(points);
    free(centr);

    return 0;
}