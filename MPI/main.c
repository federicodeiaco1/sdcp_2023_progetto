#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>
#include <stddef.h>
#include "../cjson/cJSON.h"

#define RANDOM_RANGE 1000

struct Point{
    float x;
    float y;
    int color;
};
typedef struct Point Point;

float distance(Point *a, Point *b){
    return sqrt((b->x - a->x)*(b->x - a->x) + (b->y - a->y)*(b->y - a->y));
}

short int end(Point *centr, Point *oldK, int k, int epoch, int epochLimit){
    short int stop = 1;
    for(int i = 0; i < k && stop; i++){
        if(fabs(centr[i].x - oldK[i].x) > 0.01 || fabs(centr[i].y - oldK[i].y) > 0.01)
            stop = 0;
    }

    if(epochLimit != __INT32_MAX__ && epoch > epochLimit)
        stop = 1;

    return stop;
}

float float_rand(float min, float max){
    float scale = rand() / (float) RAND_MAX;
    return min + scale * (max - min);
}

void writeJSON(Point *points, int n, Point *centr, int k){
    cJSON *globalObj = cJSON_CreateObject(); //global object
    cJSON *pointArr = cJSON_CreateArray();
    cJSON *pointObj;

    for(int i = 0; i < n; i++){
        pointObj = cJSON_CreateObject();
        cJSON_AddNumberToObject(pointObj, "x", points[i].x);
        cJSON_AddNumberToObject(pointObj, "y", points[i].y);
        cJSON_AddNumberToObject(pointObj, "col", points[i].color);
        cJSON_AddItemToArray(pointArr, pointObj);
    }
    cJSON_AddItemToObject(globalObj, "points", pointArr);
    pointArr = cJSON_CreateArray();

    for(int i = 0; i < k; i++){
        pointObj = cJSON_CreateObject();
        //printf("x:%f\ty:%f\tcolor:%d\n", centr[i].x, centr[i].y, centr[i].color);
        cJSON_AddNumberToObject(pointObj, "x", centr[i].x);
        cJSON_AddNumberToObject(pointObj, "y", centr[i].y);
        cJSON_AddNumberToObject(pointObj, "col", centr[i].color);
        cJSON_AddItemToArray(pointArr, pointObj);
    }
    cJSON_AddItemToObject(globalObj, "centr", pointArr);

    char *json_str = cJSON_Print(globalObj);

    FILE *fp = fopen("../points.json", "w");
    if(fp == NULL){
        printf("Error: Unable to open the file.\n");
    }

    fputs(json_str, fp);
    fclose;
    cJSON_free(json_str);
    cJSON_Delete(globalObj);
}


int main(int argc, char *argv[]){

    MPI_Init(NULL, NULL);
    int rank, size, root = 0;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    //Creo un nuovo tipo per poter fare le operazioni di comunicazione con MPI
    const int nitems=3;
    int blocklengths[] = {1,1,1};
    MPI_Datatype types[] = {MPI_INT, MPI_INT, MPI_FLOAT};
    MPI_Datatype mpi_point_type;
    MPI_Aint offsets[nitems];

    offsets[0] = offsetof(Point, x);
    offsets[1] = offsetof(Point, y);
    offsets[2] = offsetof(Point, color);

    MPI_Type_create_struct(nitems, blocklengths, offsets, types, &mpi_point_type);
    MPI_Type_commit(&mpi_point_type);



    int n; //numero di punti
    int k; //numero di centroidi
    int epochLimit = -1;

    Point *points; //Puntatore all'array dei punti
    Point *centr; //Puntatore all'array dei centroidi
    if(rank == root){

        if(argc > 1){
            n = atoi(argv[1]);
            k = atoi(argv[2]);
            if(argc == 4)
                epochLimit = atoi(argv[3]);
        }else{
            printf("Quanti punti si vuole generare?\n");
            scanf("%d", &n);

            printf("Quanti centroidi si vogliono?\n");
            scanf("%d", &k);

            printf("Inserire limite massimo di iterazioni (-1 se non si vuole nessun limite)\n");
            scanf("%d", &epochLimit);
        }
        points = calloc(n, sizeof(Point));
        centr = calloc(k, sizeof(Point));

        //Manda n e k a tutti
        MPI_Bcast(&n, 1, MPI_INT, root, MPI_COMM_WORLD);
        MPI_Bcast(&k, 1, MPI_INT, root, MPI_COMM_WORLD);

        if(epochLimit == -1)
            epochLimit = __INT_MAX__;
        
        //Generazione punti
        for(int i = 0; i < n; i++){
            points[i].x = float_rand(-RANDOM_RANGE, RANDOM_RANGE);
            points[i].y = float_rand(-RANDOM_RANGE, RANDOM_RANGE);
        }

        //Scelgo i centroidi a caso tra i punti generati
        for(int i = 0; i < k; i++){
            int selected = (rand() % ((n-1) - (0) + 1)) + (0);
            centr[i].x = points[selected].x;
            centr[i].y = points[selected].y;
            centr[i].color = i + 1;
        }

        //Mando a tutti i centroidi
        MPI_Bcast(centr, k, mpi_point_type, root, MPI_COMM_WORLD);
    }else{
        //Ricevi il numero totale di punti e il numero di centroidi
        MPI_Bcast(&n, 1, MPI_INT, root, MPI_COMM_WORLD);
        MPI_Bcast(&k, 1, MPI_INT, root, MPI_COMM_WORLD);
        
        centr = calloc(k, sizeof(Point));
        //Ricevo i centroidi dal root
        MPI_Bcast(centr, k, mpi_point_type, root, MPI_COMM_WORLD);
    }

    Point *mySubPoints;
    if(rank == size - 1 && n % size != 0){
        mySubPoints = calloc(n/size + n%size, sizeof(Point));
    }else{
        mySubPoints = calloc(n/size, sizeof(Point));
    }

    int sendCount[size], displs[size];
    for(int i = 0; i < size; i++){
        displs[i] = (n/size) * i;
        sendCount[i] = n/size;
        if(i == size - 1)
            sendCount[i] += n%size;
    }

    if(rank == root){
        //Se root manda i sottoinisiemi di punti
        MPI_Scatterv(points, sendCount, displs, mpi_point_type, mySubPoints, sendCount[rank], mpi_point_type, root, MPI_COMM_WORLD);
    }else{
        //Se non root ricevi i sottoinsiemi di punti
        MPI_Scatterv(NULL, NULL, NULL, mpi_point_type, mySubPoints, sendCount[rank], mpi_point_type, root, MPI_COMM_WORLD);
    }

    
    //Adesso tutti i processi hanno i centroidi di partenza e il loro sottoinsieme di punti
    
    float minDistance, newDistance; //distanza minima e distanze nuova calcolata dalla funzione distance()
    float *mySubSumCoordX, *mySubSumCoordY; //Somma dei valori delle cordinate, es. in sumCoordX[0] c'è la somma di tutte le x di tutti i punti che hanno colore 1
    //                                                                        in sumCoordX[1] c'è la somma di tutte le x di tutti i punti che hanno colore 2
    //                                  stessa roba per sumCoordY.
    int *mySubPointNumber; //Numero di punti, in posizione i c'e il numero di punti che hanno colore i + 1

    mySubSumCoordX = calloc(k, sizeof(float));
    mySubSumCoordY = calloc(k, sizeof(float));
    mySubPointNumber = calloc(k, sizeof(int));

    int epoch = 1;
    short int stop = 0;
    int mySubN = n/size; //Numero di punti che ha ogni processo
    if(rank == root){
        Point *oldK = calloc(k, sizeof(Point));
        stop = end(centr, oldK, k, epoch, epochLimit);

        //Questi array sono quelli che ottengono il risultato della reduce degli array mySubSumCoordX, mySubSumCoordY e mySubPointNumber
        float *sumCoordX, *sumCoordY;
        int *pointNumber;

        sumCoordX = calloc(k, sizeof(float));
        sumCoordY = calloc(k, sizeof(float));
        pointNumber = calloc(k, sizeof(int));

        while(!stop){
            //Mi salvo all'inizio di ogni iterazione il valore dei centroidi, così
            //posso controllare se sono cambiati (e se non dovessero cambiare fine di k means)
            for(int i = 0; i < k; i++){
                oldK[i].x = centr[i].x;
                oldK[i].y = centr[i].y;
            }

            //Per ogni punto devo calcolare la distanza da ogni centroide e selezionare
            //quello a distanza minore
            for(int i = 0; i < mySubN; i++){
                minDistance = __INT32_MAX__;
                for(int j = 0; j < k; j++){
                    newDistance = distance(&mySubPoints[i], &centr[j]);
                    if(newDistance < minDistance){
                        mySubPoints[i].color = centr[j].color;
                        minDistance = newDistance;
                    }
                }
            }

            //Valori per aggiornamento dei centroidi
            for(int i = 0; i < mySubN; i++){
                mySubSumCoordX[mySubPoints[i].color - 1] += mySubPoints[i].x;
                mySubSumCoordY[mySubPoints[i].color - 1] += mySubPoints[i].y;
                mySubPointNumber[mySubPoints[i].color - 1]++;
            }


            //Recupero i valori per aggiornamento dei centroidi dagli altri processi non root
            MPI_Reduce(mySubSumCoordX, sumCoordX, k, MPI_FLOAT, MPI_SUM, root, MPI_COMM_WORLD);
            MPI_Reduce(mySubSumCoordY, sumCoordY, k, MPI_FLOAT, MPI_SUM, root, MPI_COMM_WORLD);
            MPI_Reduce(mySubPointNumber, pointNumber, k, MPI_INT, MPI_SUM, root, MPI_COMM_WORLD);

            //Azzero gli array per la prossima iterazione
            for (int i = 0; i < k; i++){
                mySubSumCoordX[i] = 0;
                mySubSumCoordY[i] = 0;
                mySubPointNumber[i] = 0;
            }
            //Aggiornamento dei centroidi
            for (int i = 0; i < k; i++){
                //printf("sumcord_i: %f, poinNumb_i: %d\n\n", sumCoordX[i], pointNumber[i]);
                if(pointNumber[i] > 0){
                    centr[i].x = sumCoordX[i]/pointNumber[i];
                    centr[i].y = sumCoordY[i]/pointNumber[i];
                    sumCoordX[i] = 0;
                    sumCoordY[i] = 0;
                    pointNumber[i] = 0;
                }
            }

            //Invio a tutti i centroidi aggiornati
            MPI_Bcast(centr, k, mpi_point_type, root, MPI_COMM_WORLD);
            

            printf("[root] Ended epoch %d\n", epoch);
            epoch++;
            stop = end(centr, oldK, k, epoch, epochLimit);
            MPI_Bcast(&stop, 1, MPI_SHORT_INT, root, MPI_COMM_WORLD);
        }

        MPI_Gatherv(mySubPoints, mySubN, mpi_point_type, points, sendCount, displs, mpi_point_type, root, MPI_COMM_WORLD);
        
        if (n < 50000) //Limite per scrivere il json, altrimenti usa troppa memoria
            writeJSON(points, n, centr, k);
        
        free(oldK);
        free(points);
        free(sumCoordX);
        free(sumCoordY);
        free(pointNumber);
    }else{
        if(rank == size - 1) //L'ultimo processo avrà sempre i punti restanti qualora n/size non sia intero
            mySubN += n%size;

        while(!stop){
            //Per ogni punto devo calcolare la distanza da ogni centroide e selezionare
            //quello a distanza minore
            for(int i = 0; i < mySubN; i++){
                minDistance = __INT32_MAX__;
                for(int j = 0; j < k; j++){
                    newDistance = distance(&mySubPoints[i], &centr[j]);
                    if(newDistance < minDistance){
                        mySubPoints[i].color = centr[j].color;
                        minDistance = newDistance;
                    }
                }
            }

            //Valori per aggiornamento dei centroidi da mandare a root
            for(int i = 0; i < mySubN; i++){
                mySubSumCoordX[mySubPoints[i].color - 1] += mySubPoints[i].x;
                mySubSumCoordY[mySubPoints[i].color - 1] += mySubPoints[i].y;
                mySubPointNumber[mySubPoints[i].color - 1]++;
            }

            //Invio dei valori di aggiornamento dei centroidi a root
            MPI_Reduce(mySubSumCoordX, NULL, k, MPI_FLOAT, MPI_SUM, root, MPI_COMM_WORLD);
            MPI_Reduce(mySubSumCoordY, NULL, k, MPI_FLOAT, MPI_SUM, root, MPI_COMM_WORLD);
            MPI_Reduce(mySubPointNumber, NULL, k, MPI_INT, MPI_SUM, root, MPI_COMM_WORLD);

            //Azzero gli array per la prossima iterazione
            for (int i = 0; i < k; i++){
                mySubSumCoordX[i] = 0;
                mySubSumCoordY[i] = 0;
                mySubPointNumber[i] = 0;
            }

            //Ricevo i centroidi aggiornati dal root
            MPI_Bcast(centr, k, mpi_point_type, root, MPI_COMM_WORLD);

            MPI_Bcast(&stop, 1, MPI_SHORT_INT, root, MPI_COMM_WORLD);
        }

        MPI_Gatherv(mySubPoints, mySubN, mpi_point_type, NULL, NULL, NULL, mpi_point_type, root, MPI_COMM_WORLD);
    }

    free(centr);
    free(mySubPoints);
    free(mySubSumCoordX);
    free(mySubSumCoordY);
    free(mySubPointNumber);
    MPI_Type_free(&mpi_point_type);
    MPI_Finalize();
}