import numpy as np
from matplotlib import pyplot as plt

def execution_time(y, name):
    x = np.array([100, 1000, 10000, 100000, 1000000, 10000000, 100000000])
    plt.plot(x, y, marker = 'o')

    plt.title(name + " execution time")
    plt.xlabel("Number of points")
    plt.ylabel("Time[s]")
    plt.yscale('log')
    plt.xscale('log')

    plt.show()

def MPI_OMP_speedup(y, name):
    x = np.array([1, 2, 3, 4, 5, 6, 7, 8])

    if name == "MPI":
        txt = "MPI speedup"
    else:
       txt = "OMP speedup"

    plt.plot(x, y, marker = 'o', label = txt)

    plt.plot(np.array([1, 2, 3, 4, 5, 6, 7, 8]), np.array([1, 2, 3, 4, 5, 6, 7, 8]), marker = 'o', label = "Ideal speedup")

    plt.title(txt)
    if name == "MPI":
        plt.xlabel("Number of processes")
    else:
       plt.xlabel("Number of threads") 
    plt.ylabel("Speedup")
    plt.legend()
    plt.show()

def MPI_OMP_efficiency(y, name):
    x = np.array([1, 2, 3, 4, 5, 6, 7, 8])

    if name == "MPI":
        txt = "MPI efficiency"
    else:
       txt = "OMP efficiency"

    plt.plot(x, y, marker = 'o', label = txt)

    plt.plot(np.array([1, 2, 3, 4, 5, 6, 7, 8]), np.array([1, 1, 1, 1, 1, 1, 1, 1]), marker = 'o', label = "Ideal efficiency")

    plt.title(txt)
    if name == "MPI":
        plt.xlabel("Number of processes")
    else:
       plt.xlabel("Number of threads") 
    plt.ylim(0, 1.3)
    plt.ylabel("Efficiency")
    plt.legend()
    plt.show()

def execution_time_block(y):
    x = np.array([4, 8, 16, 32, 64, 128, 256, 512, 1024])
    Nb = ["2500000", "1250000", "625000", "312500", "156250", "78125", "39063", "19494", "9766"]

    plt.plot(x, y, marker = 'o')
    idx = 0
    for i,j in zip(x, y):
        if idx != 5:
            plt.annotate("NBlocks=" + Nb[idx],
                        (i,j),
                        textcoords="offset points",
                        xytext=(0,10),
                        ha = "center")
        else:
            plt.annotate("NBlocks=" + Nb[idx],
                        (i,j),
                        textcoords="offset points",
                        xytext=(0,-10),
                        ha = "center")
        idx = idx + 1

    plt.title("Execution time with increasing thread per block")
    plt.xlabel("Number of threads per block")
    plt.ylabel("Time[s]")

    plt.show()

def all_execution_time(y_serial, y_mpi, y_omp, y_cuda):
    x = np.array([100, 1000, 10000, 100000, 1000000, 10000000, 100000000])

    plt.plot(x, y_serial, marker = 'o', label = "serial time")
    plt.plot(x, y_mpi, marker = 'o', label = "MPI time")
    plt.plot(x, y_omp, marker = 'o', label = "openMP time")
    plt.plot(x, y_cuda, marker = 'o', label = "cuda time")

    plt.title("All execution time")
    plt.xlabel("Number of points")
    plt.ylabel("Time[s]")
    plt.yscale('log')
    plt.xscale('log')
    plt.legend()
    plt.show()


#serial execution time
y_serial = np.array([0.013, 0.016, 0.054, 0.132, 1.250, 12.157, 126.742])
execution_time(y_serial, "Serial")


#MPI execution time
y_mpi = np.array([0.343, 0.296, 0.362, 0.370, 0.588, 2.642, 20.544])
execution_time(y_mpi, "MPI")

#MPI speedup each value already contains the value T_s/T_p where T_s is always
#equal to 8.334 and here the T_p values (times with n=40000 and k=800):
#np = 1 --> 8.549
#np = 2 --> 4.739
#np = 3 --> 3.587
#np = 4 --> 2.885 
#np = 5 --> 2.744
#np = 6 --> 2.494
#np = 7 --> 2.409
#np = 8 --> 2.184
y = np.array([0.97, 1.76, 2.32, 2.89, 3.04, 3.34, 3.46, 3.82])
MPI_OMP_speedup(y, "MPI")

#MPI efficiency calculated on previous speedups
y= np.array([0.97, 0.88, 0.77, 0.72, 0.61, 0.57, 0.49, 0.48])
MPI_OMP_efficiency(y, "MPI")



#OMP execution time
y_omp = np.array([0.005, 0.007, 0.030, 0.032, 0.224, 1.995, 20.207])
execution_time(y_omp, "OMP")

#OMP speedup each value already contains the value T_s/T_p where T_s is always
#equal to 8.334 and here the T_p values (times with n=40000 and k=800):
#n_th = 1 --> 14.716
#n_th = 2 --> 5.084
#n_th = 3 --> 3.223
#n_th = 4 --> 3.334 
#n_th = 5 --> 2.396
#n_th = 6 --> 2.187
#n_th = 7 --> 2.136
#n_th = 8 --> 1.873
y = np.array([0.57, 1.64, 2.59, 2.5, 3.48, 3.81, 3.90, 4.45])
MPI_OMP_speedup(y, "OMP")

#OMP efficiency calculated on previous speedups
y= np.array([0.57, 0.82, 0.86, 0.63, 0.70, 0.64, 0.56, 0.56])
MPI_OMP_efficiency(y, "OMP")

#CUDA execution time
y_cuda = np.array([0.198, 0.154, 0.152, 0.144, 0.188, 0.630, 4.922])
execution_time(y_cuda, "CUDA")

#CUDA execution time changing block per thread with n = 10 000 000 and k = 800
y = np.array([534.86, 305.97, 206.32, 77.20, 59.55, 59.12, 59.10, 61.33, 48.87])
execution_time_block(y)

#All execution times together
all_execution_time(y_serial, y_mpi, y_omp, y_cuda)
