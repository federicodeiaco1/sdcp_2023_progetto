# SdCP project A.Y. 2022/2023

This project aims to present an implementation of the k-means clustering algorithm on 2-D points using three different technologies for parallel code implementation in C:
* MPI
* openMP
* CUDA

is also implemented the serial version of the algorithm in order to show the differences with the parallel versions.

# How to run the project

## Serial version
Compile it inside the **serial** folder with:
```sh 
gcc ../cjson/cJSON.c main.c -lm
```
and then run it with:
```sh 
./a.out
```
you can also specify the number of points, centroids and epochs with:
```sh 
./a.out 2500 4 10
```

## MPI version
Compile it inside the **MPI** folder with:
```sh 
mpicc ../cjson/cJSON.c main.c -lm
```
and then run it with:
```sh 
mpirun -np 8 a.out
```
you can also specify the number of points, centroids and epochs with:
```sh 
mpirun -np 8 a.out 2500 4 10
```

## openMP version
Compile it inside the **openMP** folder with:
```sh 
gcc -fopenmp ../cjson/cJSON.c main.c -lm
```
and then run it with:
```sh 
./a.out
```
you can also specify the number of points, centroids and epochs with:
```sh 
./a.out 2500 4 10
```

**IMPORTANT**: in order to compile and run the code correctly you need at least the openMP version 4.5

## CUDA version
Compile it inside the **openMP** folder with:
```sh 
nvcc ../cjson/cJSON.c main.c -lm
```
and then run it with:
```sh 
./a.out
```
you can also specify the number of points, centroids and epochs with:
```sh 
./a.out 2500 4 10
```

## See the result
When you run a program it will generate a file named *point.json* inside the root folder if the number of points is less than 50000.<br>
You can then run the **app.py** script in order to see the result of the program.