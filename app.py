from matplotlib import pyplot as plt
import json
import random

f = open("points.json")

data = json.load(f)

f.close()

color = ["#"+''.join([random.choice('0123456789ABCDEF') for j in range(6)])
             for i in range(len(data["centr"]))]

#Building points array
i = 1
while i <= len(data["centr"]):
    x = list()
    y = list()
    for point in data["points"]:
        if(point["col"] == i):
            x.append(point["x"])
            y.append(point["y"])
    plt.scatter(x, y, color = color[i - 1])
    plt.scatter(data["centr"][i - 1]["x"], data["centr"][i - 1]["y"], color = color[i - 1], s=300)
    print(i, "centr done")
    i += 1

plt.show()
