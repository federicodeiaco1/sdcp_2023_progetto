#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
#include "../cjson/cJSON.h"

#define RANDOM_RANGE 1000
#define N_TH 8

struct Point{
    float x;
    float y;
    int color;
};
typedef struct Point Point;

float distance(Point *a, Point *b){
    return sqrt((b->x - a->x)*(b->x - a->x) + (b->y - a->y)*(b->y - a->y));
}

short int end(Point *centr, Point *oldK, int k){
    short int stop = 1;
    for(int i = 0; i < k && stop; i++){
        //printf("centr[%d].x=%f old[%d].x=%f centr[%d].y=%f old[%d].y=%f\n",i, roundf(centr[i].x*100)/100, i, roundf(oldK[i].x*100)/100, i, roundf(centr[i].y*100)/100, i, roundf(oldK[i].y*100)/100);
        if(fabs(centr[i].x - oldK[i].x) > 0.01 || fabs(centr[i].y - oldK[i].y) > 0.01)
            stop = 0;
    }
    return stop;
}

float float_rand(float min, float max, unsigned int *seed){
    float scale = rand_r(seed) / (float) RAND_MAX;
    return min + scale * (max - min);
}

void writeJSON(Point *points, int n, Point *centr, int k){
    cJSON *globalObj = cJSON_CreateObject(); //global object
    cJSON *pointArr = cJSON_CreateArray();
    cJSON *pointObj;

    for(int i = 0; i < n; i++){
        pointObj = cJSON_CreateObject();
        cJSON_AddNumberToObject(pointObj, "x", points[i].x);
        cJSON_AddNumberToObject(pointObj, "y", points[i].y);
        cJSON_AddNumberToObject(pointObj, "col", points[i].color);
        cJSON_AddItemToArray(pointArr, pointObj);
    }
    cJSON_AddItemToObject(globalObj, "points", pointArr);
    pointArr = cJSON_CreateArray();

    for(int i = 0; i < k; i++){
        pointObj = cJSON_CreateObject();
        //printf("x:%f\ty:%f\tcolor:%d\n", centr[i].x, centr[i].y, centr[i].color);
        cJSON_AddNumberToObject(pointObj, "x", centr[i].x);
        cJSON_AddNumberToObject(pointObj, "y", centr[i].y);
        cJSON_AddNumberToObject(pointObj, "col", centr[i].color);
        cJSON_AddItemToArray(pointArr, pointObj);
    }
    cJSON_AddItemToObject(globalObj, "centr", pointArr);

    char *json_str = cJSON_Print(globalObj);

    FILE *fp = fopen("../points.json", "w");
    if(fp == NULL){
        printf("Error: Unable to open the file.\n");
    }

    fputs(json_str, fp);
    fclose;
    cJSON_free(json_str);
    cJSON_Delete(globalObj);
}

int main(int argc, char *argv[]){
    
    int k; //Numero di centroidi
    int n; //Numero di punti
    int epochLimit = -1;

    if(argc > 1){
        n = atoi(argv[1]);
        k = atoi(argv[2]);
        if(argc == 4)
            epochLimit = atoi(argv[3]);
    }else{
        printf("Quanti punti si vuole generare?\n");
        scanf("%d", &n);

        printf("Quanti centroidi si vogliono?\n");
        scanf("%d", &k);

        printf("Inserire limite massimo di iterazioni (-1 se non si vuole nessun limite)\n");
        scanf("%d", &epochLimit);
    }

    if(epochLimit == -1)
        epochLimit = __INT_MAX__;
    
    Point *points = calloc(n, sizeof(Point));
    Point *centr = calloc(k, sizeof(Point));

    //Generazione punti (da -RAND_MAX a RAND_MAX)
    #pragma omp parallel num_threads(N_TH)
    {
        unsigned int seed = omp_get_thread_num();
        #pragma omp for
        for(int i = 0; i < n; i++){
            points[i].x = float_rand(-RANDOM_RANGE, RANDOM_RANGE, &seed);
            points[i].y = float_rand(-RANDOM_RANGE, RANDOM_RANGE, &seed);
        }
    }

    //Scelgo i centroidi a caso tra i punti generati
    #pragma omp parallel num_threads(N_TH)
    {
        unsigned int seed = omp_get_thread_num();

        #pragma omp for
        for(int i = 0; i < k; i++){
            int selected = (rand_r(&seed) % ((n-1) - (0) + 1)) + (0);
            centr[i].x = points[selected].x;
            centr[i].y = points[selected].y;
            centr[i].color = i + 1;
        }
    }
    /*printf("\nPoints:\n");
    for(int i = 0; i < n; i++){
        printf("x:%f\ty:%f\tcolor:%d\n", points[i].x, points[i].y, points[i].color);
    }*/

    Point *oldK = calloc(k, sizeof(struct Point));

    int epoch = 1;
    float *sumCoordX, *sumCoordY; //Somma dei valori delle cordinate, es. in sumCoordX[0] c'è la somma di tutte le x di tutti i punti che hanno colore 1
    //                                                                        in sumCoordX[1] c'è la somma di tutte le x di tutti i punti che hanno colore 2
    //                                  stessa roba per sumCoordY.
    int *pointNumber;//Numero di punti, in posizione i c'e il numero di punti che hanno colore i + 1
    sumCoordX = calloc(k, sizeof(float));
    sumCoordY = calloc(k, sizeof(float));
    pointNumber = calloc(k, sizeof(int));

    while(!end(centr, oldK, k) && epoch <= epochLimit){
        //Mi salvo all'inizio di ogni iterazione il valore dei centroidi, così
        //posso controllare se sono cambiati (e se non dovessero cambiare fine di k means)
        for(int i = 0; i < k; i++){
            oldK[i].x = centr[i].x;
            oldK[i].y = centr[i].y;
        }

        //Per ogni punto devo calcolare la distanza da ogni centroide e selezionare
        //quello a distanza minore
        #pragma omp parallel num_threads(N_TH) reduction(+:sumCoordX[:k]) reduction(+:sumCoordY[:k]) reduction(+:pointNumber[:k])
        {
            float minDistance, newDistance;
            //printf("im thread %d, %p\n", omp_get_thread_num(), sumCoordX);
            #pragma omp for
            for(int i = 0; i < n; i++){
                minDistance = __INT32_MAX__;
                for(int j = 0; j < k; j++){
                    newDistance = distance(&points[i], &centr[j]);
                    if(newDistance < minDistance){
                        points[i].color = centr[j].color;
                        minDistance = newDistance;
                    }
                }
            }

            //Aggiornamento dei centroidi
            #pragma omp for
            for(int i = 0; i < n; i++){
                //printf("\nim thread %d, %p\n", omp_get_thread_num(), sumCoordX);
                sumCoordX[points[i].color - 1] += points[i].x;
                sumCoordY[points[i].color - 1] += points[i].y;
                pointNumber[points[i].color - 1]++;
            }

        }

        //printf("Updating %p\n", sumCoordX);
        for (int i = 0; i < k; i++){
            //printf("sumcord_i: %f, poinNumb_i: %d\n\n", sumCoordX[i], pointNumber[i]);
            if(pointNumber[i] > 0){
                centr[i].x = sumCoordX[i]/pointNumber[i];
                centr[i].y = sumCoordY[i]/pointNumber[i];
                sumCoordX[i] = 0;
                sumCoordY[i] = 0;
                pointNumber[i] = 0;
            }
        }

        /*printf("\nCentroids:\n");
        for(int i = 0; i < k; i++){
            printf("x:%f\ty:%f\tcolor:%d\n", centr[i].x, centr[i].y, centr[i].color);
        }*/

        printf("Ended epoch %d\n", epoch);
        epoch++;
    }

    if (n < 50000) //Limite per scrivere il json, altrimenti usa troppa memoria
        writeJSON(points, n, centr, k);

    free(points);
    free(centr);
    free(oldK);

    return 0;
}